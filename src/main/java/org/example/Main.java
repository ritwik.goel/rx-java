package org.example;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import org.openjsse.util.RSAKeyUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
     //   makeObserver();
   //createObservableJust();
//        createObservableFrom();
//        createObservableEmitter();
        //multipleObservers();
        //rangeObserver();
//        deferObserver();
        //fromCallableCall();
        //pauseInterval();
        //createSingle();
        //createMaybe();
        //createDisposabel();
        mapOperator();


    }






    public static void mapOperator(){
        Observable<Integer> observable= Observable.just(1,2,3,45,56);
        observable.map(item-> item * 3).filter(item->item%2==0)
        .subscribe(System.out::println);
        //its printing the second part
    }




    public static void createDisposabel(){
        Observable observable= Observable.just(1,2,3,4);
        Observer<Integer> observer= new Observer<Integer>() {
            Disposable disposable;
            @Override
            public void onSubscribe(Disposable d) {
                disposable=d;
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println(integer);
                if( integer==3){
                    disposable.dispose();
                }

            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }











    public static void createSingle(){
        Single.just("Hello").subscribe(item-> System.out.println(item));
    }

    public static void createMaybe(){
        Maybe.empty().subscribe(new MaybeObserver<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Object o) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                System.out.println("COmpleted");
            }
        });
    }
    public static void pauseInterval(){
        Observable observable= Observable.interval(1, TimeUnit.SECONDS);
        observable.subscribe(item-> System.out.println(item));

    }

    //good for errors. Uses lazy loading
    public static void fromCallableCall(){
        Observable observable= Observable.fromCallable(()->{return getNumber();});
        observable.subscribe(item-> System.out.println(item), System.out::println);
    }

    public static int getNumber(){
        return 1+1;
    }

    public static void deferObserver(){
        int start=2;
        int count=3;
        int finalCount = count;
        Observable observable= Observable.defer(()->Observable.range(start, finalCount));
        observable.subscribe(System.out::println);
        observable.subscribe(System.out::println);//this will not print the updated values unless you add defer

    }


    public static void rangeObserver(){
        Observable observable= Observable.range(1,10);
        observable.subscribe(System.out::println, System.out::println);
    }

    public static void multipleObservers(){
        ConnectableObservable observable= Observable.just(1,2,3,4).publish();
        observable.subscribe(System.out::println);

        observable.connect();
        observable.subscribe(System.out::println);
        observable.connect();
    }



    public static void makeObserver(){
        Observable<Integer> observable= Observable.just(1,2,3,45);
        io.reactivex.Observer<Integer> observer= new io.reactivex.Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                System.out.println("subbed");
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("Hello");
                System.out.println(integer);
            }

            @Override
            public void onError(Throwable e) {
                System.out.println(e);
            }

            @Override
            public void onComplete() {
                System.out.println("COmpleted");
            }
        };
    }
    public static void createObservableJust(){
        Observable<Integer> observable= Observable.just(1,2,3,45);
        observable.subscribe(item-> System.out.println(item));
    }
    public static void createObservableFrom(){
        List<Integer> list= new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Observable observable= Observable.fromArray(list.toArray());
        observable.subscribe(item-> System.out.println(item));
        }
    //most imp
        public static void createObservableEmitter(){
        Observable<Integer> observable= Observable.create(emitter -> {
            emitter.onNext(1);
            emitter.onNext(3);
            emitter.onComplete();
        });
        observable.subscribe(item-> System.out.println(item),error-> System.out.println(error),()-> System.out.println("Completed"));
    }

    }
